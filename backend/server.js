const express=require('express')
const db=require("./db")
const app=express()

app.use(express.json())

// . GET  --> Display Book using the name from Containerized MySQL

// 2. POST --> ADD Book data into Containerized MySQL table

// 3. UPDATE --> Update publisher_name and Author_name into Containerized MySQL table

// 4. DELETE --> Delete Book from Containerized MySQL
app.get("/:name",(request,response)=>{

    const{name}=request.params
    const connection=db.openConnection()

    const sql=`SELECT * FROM book WHERE book_title='${name}'`

    connection.query(sql,(error,result)=>{
        connection.end()
        response.send(result)
    })
    

})

//book_id, book_title, publisher_name, author_name
app.post("/",(request,response)=>{

    const{id,title,pubname,author}=request.body
    const connection=db.openConnection()

    const sql=`INSERT INTO book VALUES('${id}','${title}','${pubname}','${author}')`
    connection.query(sql,(error,result)=>{
        connection.end()
        response.send(result)
    })
    

})


app.put("/:id",(request,response)=>{

    const{id}=request.params
    const{pubname,author}=request.body
    const connection=db.openConnection()

    const sql=`UPDATE book SET publisher_name='${pubname}' AND author_name='${author}' WHERE book_id='${id}'`
    connection.query(sql,(error,result)=>{
        connection.end()
        response.send(result)
    })
    

})

app.delete("/:id",(request,response)=>{

    const{id}=request.params
   
    const connection=db.openConnection()

    const sql=`DELETE FROM book WHERE book_id='${id}'`
    connection.query(sql,(error,result)=>{
        connection.end()
        response.send(result)
    })
    

})

app.listen(4000,()=>{

    console.log('server listeining on port 4000')
})